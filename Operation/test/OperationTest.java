/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import fi.helsinki.cs.tmc.edutestutils.Points;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author o2144696
 */

@Points("4")
public class OperationTest {
    
    @Test
    public void testAddition() {
        assertTrue("La methode d'addition ne fonctionne pas !", Operations.Addition(10,5) == 15);
        assertTrue("La methode d'addition ne fonctionne pas !", Operations.Addition(100,100) == 200);
    }
    
    @Test
    public void testMultiplication() {
        assertTrue("La methode de multiplication ne fonctionne pas !", Operations.Multiplication(10,5) == 50);
        assertTrue("La methode de multiplication ne fonctionne pas !", Operations.Multiplication(100,100) == 10000);
    }
    
    @Test
    public void testSoustraction() {
        assertTrue("La méthode de soustraction ne fonctionne pas !", Operations.Soustraction(10,5) == 5);
        assertTrue("La méthode de soustraction ne fonctionne pas !", Operations.Soustraction(100,100) == 0);
    }
    
    @Test
    public void testDivision() {
        assertTrue("La méthode de division ne fonctionne pas !", Operations.Division(10,5) == 2);
        assertTrue("La méthode de division ne fonctionne pas !", Operations.Division(100,100) == 1);
    }
}
