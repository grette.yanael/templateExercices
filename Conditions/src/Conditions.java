/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author o2144696
 */
public class Conditions {
    public static int IfPlusGrand(int a, int b) {
        // BEGIN SOLUTION
        if(a < b) {
            return b;
        }
        return a;
        // END SOLUTION
        // STUB: return -1; //ecrire la reponse ici
    }
    
   /*
   Cette méthode doit renvoyer la lettre correspondant à la valeur d'entre dans l'alphabet
   la valeur va de 1 à 5
   elle doit envoyer un message indiquant que la valeur est invalide si elle est inférieur à 1 et supérieur à 5 
   */
    public static String getLettre(int a) {
        // BEGIN SOLUTION
        switch(a) {
            case 1 : return "a";
            case 2 : return "b";
            case 3 : return "c";
            case 4 : return "d";
            case 5 : return "e";
            default : return "La valeur ne correspond pas à un chiffre de 1 à 5"; 
        }
        // END SOLUTION
        // STUB: return ""; //ecrire la reponse ici
    }
}
