/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import fi.helsinki.cs.tmc.edutestutils.Points;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author o2144696
 */
public class ConditionsTest {
    
    @Test
    @Points("1")
    public void testConditionPlusGrand() {
        assertTrue("La méthode ne renvoie pas le plus grand nombre", Conditions.IfPlusGrand(-5, 5)==5);
        assertTrue("La méthode ne renvoie pas le plus grand nombre", Conditions.IfPlusGrand(30, 5)==30);
        assertFalse("La méthode ne renvoie pas le plus grand nombre", Conditions.IfPlusGrand(42, 34)==34);
        assertFalse("La méthode ne renvoie pas le plus grand nombre", Conditions.IfPlusGrand(38, 283)==-34);
    }
    
    @Test
    @Points("3")
    public void testConditionSwitch() {
        assertTrue("La méthode ne renvoie pas la bonne valeur pour 1",Conditions.getLettre(1).equals("a"));
        assertTrue("La méthode ne renvoie pas la bonne valeur pour 2",Conditions.getLettre(2).equals("b"));
        assertTrue("La méthode ne renvoie pas la bonne valeur pour 3",Conditions.getLettre(3).equals("c"));
        assertTrue("La méthode ne renvoie pas la bonne valeur pour 4",Conditions.getLettre(4).equals("d"));
        assertTrue("La méthode ne renvoie pas la bonne valeur pour 5",Conditions.getLettre(5).equals("e"));
        assertFalse("La méthode ne doit renvoyer des valeurs que pour des entier allant de 1 à 5",Conditions.getLettre(6).equals("F"));
    }
}
