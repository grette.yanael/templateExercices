/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import fi.helsinki.cs.tmc.edutestutils.Points;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author o2144696
 */

@Points("5")
public class HashMapTest {
    
    int taille = 10;
    String [] tab = {"OK","esaaie","IUT","Info","Association","Corpus","docs","Archer","Oui","jeux"};

    @Test
    public void testHashMap() {
        java.util.HashMap<Integer, String> hm = HashMap.getHashMap(tab, taille);
        
        for(int i =0; i< taille; i++) {
            assertTrue("La valeur de la clé "+i+" ne correspond pas à la valeur attendu", hm.get(i).equals(tab[i]));
        }
    }
}
