/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import fi.helsinki.cs.tmc.edutestutils.Points;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author o2144696
 */
public class ListeTests {
    
    int tailleTab1 = 10;
    int[] tab1 = {23,34,4,243,42,9,92,44,48,0};
    int tailleTab2 = 6;
    String[] tab2 = {"Hello","World", "How","Are","You","?"};
    
    
    @Test
    @Points("2")
    public void testListeInteger(){
        
        assertTrue("Il y a un problème dans le parcours dans votre liste de int",Liste.getIndice(tab1, tailleTab1, 23) == 0);
        assertTrue("Il y a un problème dans le parcours dans votre liste de int",Liste.getIndice(tab1, tailleTab1, 44) != -1);
        
        assertTrue("Votre méthode pour les int ne renvoie pas la bonne valeur",Liste.getVal(tab1, tailleTab1, 7)==44);
        assertTrue("Votre méthode pour les int ne renvoie pas la bonne valeur",Liste.getVal(tab1, tailleTab1, tailleTab1)==-1);
    }
    @Points("2")
    public void testListeString(){
        
        assertTrue("Il y a un problème dans le parcours dans votre liste de chaine",Liste.getIndice(tab2, tailleTab2, "You") == 4 );
        assertTrue("Il y a un problème dans le parcours dans votre liste de chaine",Liste.getIndice(tab2, tailleTab2, "Hi") == -1);
        
        assertTrue("Votre méthode pour les chaines ne renvoie pas la bonne valeur",Liste.getVal(tab2, tailleTab2, 3).equals("Are"));
        assertTrue("Votre méthode ne pour les chaines renvoie pas la bonne valeur",Liste.getVal(tab2, tailleTab2, 7).length()==0);
    }
}
