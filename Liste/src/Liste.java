/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author o2144696
 */
public class Liste {
    public static int getIndice(int[] liste, int taille, int val) {
        // BEGIN SOLUTION
        for(int i =0; i< taille; ++i) {
            if( liste[i]== val) {
                return i;
            }
        }
        return -1;
        // END SOLUTION
        // STUB: return -1; //ecrire la reponse ici
    }
    
    
    public static int getIndice(String[] liste, int taille, String val) {
        // BEGIN SOLUTION
        for(int i =0; i< taille; ++i) {
            if( liste[i].equals(val)) {
                return i;
            }
        }
        return -1;
        // END SOLUTION
        // STUB: return -1; //ecrire la reponse ici
    }
    
    public static int getVal(int[] liste, int taille, int indice) {
        // BEGIN SOLUTION
        if( taille > indice && indice >= 0) {
            return liste[indice];
        }
        return -1;
        // END SOLUTION
        // STUB: return -1; //ecrire la reponse ici
    }
    
    public static String getVal(String[] liste, int taille, int indice) {
        // BEGIN SOLUTION
        if( taille > indice && indice >= 0) {
            return liste[indice];
        }
        return "";
        // END SOLUTION
        // STUB: return ""; //ecrire la reponse ici
    }
}
